# frozen_string_literal: true

require 'rack/throttle'
require 'rack'

use Rack::Throttle::Interval

use Rack::Throttle::Daily,    max: 100  # requests
use Rack::Throttle::Hourly,   max: 60   # requests
use Rack::Throttle::Second,   max: 1 # requests
