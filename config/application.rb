# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'
require 'rack/throttle'
require 'rack'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module AnswerQuestionWithApi
  class Application < Rails::Application
    config.middleware.use Rack::Throttle::Interval
    config.load_defaults 5.2
  end
end
