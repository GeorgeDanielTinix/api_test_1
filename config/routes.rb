# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api do
    resources :questions, only: %i[index]
  end

  root to: 'dashboard#index'
end
