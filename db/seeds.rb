# frozen_string_literal: true

require 'faker'

if Question.count.zero?
  provider = []
  asker = User.create(full_name: Faker::Name.name)

  5.times do
    provider << User.create(full_name: Faker::Name.name)
  end

  5.times do
    question = Question.create(question: Faker::Lorem.sentence, asker_id: asker.id)

    5.times do
      Answer.create(answer: Faker::Lorem.sentence, question_id: question.id, provider_id: provider.sample.id, private: [true, false].sample)
    end
  end
end

if Tenant.count.zero?
  5.times do
    tenant = Tenant.create(full_name: Faker::Name.name, api_key: SecureRandom.hex(16))

    puts "#{tenant.full_name} successfully created!"
  end
end
