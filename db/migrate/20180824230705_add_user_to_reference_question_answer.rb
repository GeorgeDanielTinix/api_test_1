# frozen_string_literal: true

class AddUserToReferenceQuestionAnswer < ActiveRecord::Migration[5.2]
  def change
    add_reference :questions, :asker, index: true
    add_reference :answers, :provider, index: true
  end
end
