# frozen_string_literal: true

class Api::QuestionsController < ApplicationController
  before_action :check_api_key, only: :index
  after_action :increase_tenant_request_count, only: :index

  def index
    hash_response = {}
    questions = Question.where(private: false)

    if params[:query].present?
      questions = questions.where("lower(question) like '%#{params[:query].downcase}%'")
    end

    if questions.present?
      hash_response[:questions] = questions.map do |question|
        {
          id: question.id,
          content: question.question,
          asker: question.asker.full_name,
          answers: question.answers.map do |answer|
            {
              answer: answer.answer,
              provider: answer.provider.full_name
            }
          end
        }
      end
    else
      hash_response = { warning: "No questions matched with the search query provided: #{params[:query]}" }
    end

    render json: { response: hash_response, status: 200 }
  end

  private

  def check_api_key
    api_key = params[:api_key]

    if api_key.present?
      unless Tenant.exists?(api_key: api_key)
        render json: { error: "API key: #{api_key} is invalid", status: 401 }
      end
    else
      render json: { error: 'API key must be provided', status: 401 }
    end
  end

  def increase_tenant_request_count
    tenant = Tenant.find_by_api_key(params[:api_key])
    tenant.update_attribute(:request_count, tenant.request_count + 1)
  end
end
