# frozen_string_literal: true

require 'lib/encryptation_helper'

class TenantsController < ApplicationController
  include EncryptationHelper

  before_filter :generate_api_key, only: %i[create]

  def generate_api_key
    encrypt('challenger')
  end
end
