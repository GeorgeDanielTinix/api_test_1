# frozen_string_literal: true

class DashboardController < ApplicationController
  def index
    @questions = Question.all
    @answers = Answer.all
    @users = User.all
    @request_for_tenant = Tenant.request_count
  end
end

# Require every API request to include a valid Tenant API key,
# and return an HTTP code of your choice if the request does not include a valid key.
# Track API request counts per Tenant.
