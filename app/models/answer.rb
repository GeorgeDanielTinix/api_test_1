# frozen_string_literal: true

class Answer < ApplicationRecord
  validates :answer, presence: true
  belongs_to :question
  belongs_to :provider, class_name: 'User'
end
