# frozen_string_literal: true

class Tenant < ApplicationRecord
  validates :full_name, presence: true

  def self.request_count
    all.each_with_object({}) do |tenant, tenants_request_count|
      tenants_request_count[tenant.full_name] = tenant.request_count
    end
  end
end
