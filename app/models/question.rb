# frozen_string_literal: true

class Question < ApplicationRecord
  validates :question, presence: true
  has_many :answers
  belongs_to :asker, class_name: 'User'
end
