# frozen_string_literal: true

class User < ActiveRecord::Base
  # before_create :set_api_attributes

  validates :full_name, presence: true
  has_many :questions, foreign_key: 'asker_id'
  has_many :anwers, foreign_key: 'asker_id'

  # def set_api_attributes
  #   self.api_key = SecureRandom.hex(16)
  # end
end
