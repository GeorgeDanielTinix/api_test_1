# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.0'

gem 'bootsnap', '>= 1.1.0', require: false
gem 'bootstrap', '~> 4.0.0.alpha3'
gem 'coffee-rails', '~> 4.2'
gem 'faker', '~> 1.9', '>= 1.9.1'
gem 'jbuilder', '~> 2.5'
gem 'jquery-rails'
gem 'rack-throttle', '~> 0.5.0'
gem 'rails', '~> 5.2.1'
gem 'sass-rails', '~> 5.0'
gem 'turbolinks', '~> 5'
gem 'uglifier', '>= 1.3.0'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'pry'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'rack', '~> 2.0'
  gem 'rubocop'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'sqlite3'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'capybara', '~> 3.6'
  gem 'chromedriver-helper'
  gem 'rspec-rails', '~> 3.8'
  gem 'selenium-webdriver'
end

group :production do
  gem 'pg'
  gem 'rails_12factor'
end

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
