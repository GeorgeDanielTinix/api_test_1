# Answer and Question Challenger

### Requirements

* Ruby version 2.5.0

* Rails version 5.2.1

* Database Sqlite3

### Installing

```
$bundler

rake db:create && rake db:migrate
rake db:seed
```

##Running testing

bundler exec rspec

## Authentication_token for API 
api_key : 

## Author 
* ** Daniel Tinivella** - Repository - https://bitbucket.org/GeorgeDanielTinix/api_test_1/src/master/